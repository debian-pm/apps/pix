Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pix
Source: https://github.com/maui-project/pix

Files: *
Copyright: 2018, Camilo Higuita
License: GPL-3+

Files: debian/*
Copyright: 2018, Jonah Brüchert <jbb@kaidan.im>
License: GPL-2+

Files: exiv2/config/aclocal.m4
  exiv2/config/ltmain.sh
Copyright: 1996-2001, 2003-2008, Free Software Foundation, Inc.
License: GPL-2+

Files: exiv2/config/config.mk.in
Copyright: 2004-2015, Andreas Huggel <ahuggel@gmx.net>
License: BSD-3-clause

Files: exiv2/configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 Please fill license FSFUL from header of exiv2/configure

Files: exiv2/contrib/coverity.sh
Copyright: 2013-2015, Gilles Caulier, <caulier dot gilles at gmail dot com>
License: GPL-3+

Files: exiv2/contrib/organize/*
Copyright: 2009, Brad Schick <schickb@gmail.com>
License: GPL-2+

Files: exiv2/contrib/organize/MD5.cpp
  exiv2/contrib/organize/MD5.h
Copyright: 2018, Camilo Higuita
License: public-domain
 Please fill license public-domain from header of exiv2/contrib/organize/MD5.cpp exiv2/contrib/organize/MD5.h

Files: exiv2/doc/include/*
Copyright: 1998-2002, Erik Arvidsson |
License: GPL
 Please fill license GPL from header of exiv2/doc/include/*

Files: exiv2/doc/include/sortabletable.css
Copyright: 2018, Camilo Higuita
License: GPL-3+

Files: exiv2/include/*
Copyright: 2004-2017, Andreas Huggel <ahuggel@gmx.net>
License: GPL-2+

Files: exiv2/include/exiv2/config.h
  exiv2/include/exiv2/exv_msvc-webready.h
  exiv2/include/exiv2/exv_msvc.h
  exiv2/include/exiv2/http.hpp
  exiv2/include/exiv2/ini.hpp
  exiv2/include/exiv2/svn_version.h
Copyright: 2018, Camilo Higuita
License: GPL-3+

Files: exiv2/msvc/tools/python/*
Copyright: Nathan Hurst 2001
License: GPL-3+

Files: exiv2/msvc/tools/python/depend.py
Copyright: 2018, Camilo Higuita
License: GPL-3+

Files: exiv2/msvc/tools/rm/*
Copyright: 2011, 2012, by John Ford
License: MPL-2.0

Files: exiv2/msvc2003/*
Copyright: 2004-2017, Andreas Huggel <ahuggel@gmx.net>
License: GPL-2+

Files: exiv2/po/Makevars
Copyright: for their translations to this person
License: GPL-3+

Files: exiv2/samples/Jzon.cpp
  exiv2/samples/Jzon.h
Copyright: 2013, Johannes Häggqvist
License: Expat

Files: exiv2/samples/iotest.cpp
  exiv2/samples/metacopy.cpp
  exiv2/samples/metacopy.hpp
  exiv2/samples/toexv.cpp
  exiv2/samples/toexv.hpp
Copyright: 2004-2017, Andreas Huggel <ahuggel@gmx.net>
License: GPL-2+

Files: exiv2/src/*
Copyright: 2004-2017, Andreas Huggel <ahuggel@gmx.net>
License: GPL-2+

Files: exiv2/src/TODO
  exiv2/src/crwedit.cpp
  exiv2/src/crwparse.cpp
  exiv2/src/doxygen.hpp.in
  exiv2/src/exiv2.1
  exiv2/src/fff.h
  exiv2/src/ini.cpp
  exiv2/src/mrwthumb.cpp
  exiv2/src/svn_version.h.in
  exiv2/src/svn_version.sh
  exiv2/src/tiff-test.cpp
  exiv2/src/tiffmn-test.cpp
  exiv2/src/timegm.h
  exiv2/src/utiltest.cpp
  exiv2/src/xmpdump.cpp
Copyright: 2018, Camilo Higuita
License: GPL-3+

Files: exiv2/src/getopt_win32.c
Copyright: 2000, The NetBSD Foundation, Inc.
License: BSD-4-clause

Files: exiv2/src/getopt_win32.h
Copyright: 1987, 1993, 1994, 1996, The Regents of the University of California.
License: BSD-4-clause

Files: exiv2/src/localtime.c
  exiv2/src/private.h
  exiv2/src/tzfile.h
Copyright: 2018, Camilo Higuita
License: public-domain
 Please fill license public-domain from header of exiv2/src/localtime.c exiv2/src/private.h exiv2/src/tzfile.h

Files: exiv2/src/nikonmn.cpp
Copyright: 2005-2014, Robert Rottmerhusen <lens_id@rottmerhusen.com>
  2004-2017, Andreas Huggel <ahuggel@gmx.net>
License: GPL-2+

Files: exiv2/xmpsdk/*
Copyright: 2002-2008, Adobe Systems Incorporated
License: GPL-3+

Files: exiv2/xmpsdk/include/MD5.h
Copyright: 2018, Camilo Higuita
License: public-domain
 Please fill license public-domain from header of exiv2/xmpsdk/include/MD5.h

Files: exiv2/xmpsdk/src/MD5.cpp
Copyright: 2018, Camilo Higuita
License: public-domain
 Please fill license public-domain from header of exiv2/xmpsdk/src/MD5.cpp

License: BSD-3-clause
 This software is Copyright (c) 2019 by foo.
 .
 This is free software, licensed under:
 .
   The (three-clause) BSD License
 .
 The BSD License
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
 .
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution. 
 .
   * Neither the name of foo nor the names of its
     contributors may be used to endorse or promote products derived from
     this software without specific prior written permission. 
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-4-clause
 Please fill license BSD-4-clause from header of exiv2/src/getopt_win32.c

License: Expat
 This software is Copyright (c) 2019 by foo.
 .
 This is free software, licensed under:
 .
   The MIT (X11) License
 .
 The MIT License
 .
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated
 documentation files (the "Software"), to deal in the Software
 without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to
 whom the Software is furnished to do so, subject to the
 following conditions:
 .
 The above copyright notice and this permission notice shall
 be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT
 WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 dated June, 2007, or (at
 your option) any later version.
 On Debian systems, the complete text of version 3 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-3'.

License: MPL-2.0
 This software is Copyright (c) 2019 by foo.
 .
 This is free software, licensed under:
 .
   Mozilla Public License Version 2.0
 .
 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0.
 On Debian systems, the complete text of Mozilla Public License v 2.0
 can be found in '/usr/share/common-licenses/MPL-2.0'.
